﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;
namespace SimpleBinderByFoxPro
{
    public partial class Form1 : Form
    {
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(string lpClassName,
     string lpWindowName);
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        const int PREVIOUS_HOTKEY_ID = 0;
        const int NEXT_HOTKEY_ID = 1;

        static string className = "Grand theft auto San Andreas";
        static string windowName = "GTA:SA:MP";

        static InputSimulator sim;
        static List<string> text;
        static int currentTextRowId = 0;
        const int WM_HOTKEY = 0x0312;
        BinderConfig config;
        public Form1()
        {
            InitializeComponent();
            Task.Run(async () =>
            {
                await AntiAFK();
            });
        }
        private bool FindAndSelectGTAWindow()
        {
            IntPtr Handle = FindWindow(className, windowName);
            if (Handle == IntPtr.Zero) return false;
            SetForegroundWindow(Handle);
            return true;
        }
        public void MakeRealisticPress(VirtualKeyCode keyCode)
        {
            sim.Keyboard.KeyDown(keyCode);
            Task.Delay(500).Wait();
            sim.Keyboard.KeyUp(keyCode);
        }
        private void MakeRealisticPress(VirtualKeyCode keyCode, int duration)
        {
            sim.Keyboard.KeyDown(keyCode);
            Task.Delay(duration).Wait();
            sim.Keyboard.KeyUp(keyCode);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sim = new InputSimulator();
        }

        protected override void WndProc(ref Message m)
        {
            /*0x0312*/
            if (m.Msg == 786)
            {
                int wParam = m.WParam.ToInt32();
                if (wParam == PREVIOUS_HOTKEY_ID) SendPreviousString();
                if (wParam == NEXT_HOTKEY_ID) SendNextString();
            }
            base.WndProc(ref m);
        }
        private void SendNextString()
        {
            if (text == null) return;
            if (currentTextRowId + 1 < text.Count)
                ++currentTextRowId;
            else currentTextRowId = 0;
            SendString(text[currentTextRowId]);
        }
        private void SendPreviousString()
        {
            if (text == null) return;
            if (currentTextRowId >= 1)
                --currentTextRowId;
            else currentTextRowId = text.Count - 1;
            SendString(text[currentTextRowId]);
        }
        private void Log(string text)
        {
            logLabel.Text = "Log: " + text;
        }
        private void SendString(string str)
        {
            if (!FindAndSelectGTAWindow()) return;
            Thread.Sleep(150);
            var _clipboardData = Clipboard.GetDataObject();

            sim.Keyboard.KeyPress(VirtualKeyCode.F6);

            sim.Keyboard.KeyDown(VirtualKeyCode.CONTROL);
            sim.Keyboard.KeyPress(VirtualKeyCode.VK_A);
            sim.Keyboard.KeyUp(VirtualKeyCode.CONTROL);

            sim.Keyboard.KeyPress(VirtualKeyCode.DELETE);

            Clipboard.SetText(str);
            Thread.Sleep(50);

            sim.Keyboard.KeyDown(VirtualKeyCode.CONTROL);
            MakeRealisticPress(VirtualKeyCode.VK_V);
            sim.Keyboard.KeyUp(VirtualKeyCode.CONTROL);

            if (enterCheckBox.Checked)
                sim.Keyboard.KeyUp(VirtualKeyCode.ACCEPT);

            Clipboard.SetDataObject(_clipboardData);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            OpenTextFile();
        }
        private void OpenTextFile()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                text = File.ReadAllLines(openFileDialog1.FileName).Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Trim()).ToList();
                SetBinderFieldText(text);
                textFileName.Text = "Фаил: " + openFileDialog1.FileName;
            }
        }
        private void SetBinderFieldText(List<string> text)
        {
            binderTextField.Text = "";
            for (int i = 0; i < text.Count; i++)
                binderTextField.AppendText(text[i] + Environment.NewLine);
            binderTextField.Select(0, 0);
            binderTextField.ScrollToCaret();
        }
        private void binderButtonPreviousField_KeyDown(object sender, KeyEventArgs e)
        {
            if (RegisterKeyFromInput(e, PREVIOUS_HOTKEY_ID))
                binderPreviousButtonField.Text = new KeysConverter().ConvertToString(e.KeyData);
        }
        private void binderNextButtonField_KeyDown(object sender, KeyEventArgs e)
        {
            if (RegisterKeyFromInput(e, NEXT_HOTKEY_ID))
                binderNextButtonField.Text = new KeysConverter().ConvertToString(e.KeyData);
        }

        private bool RegisterKeyFromInput(KeyEventArgs e, int Hotkey_ID)
        {
            UnregisterHotKey(this.Handle, Hotkey_ID);
            Keys modifierKeys = e.Modifiers;

            Keys pressedKey = e.KeyData ^ modifierKeys;
            if (pressedKey == Keys.Menu || pressedKey.ToString() == modifierKeys.ToString() + "Key") return false;

            int m = GetModifierKeysID(modifierKeys);

            int k = (int)pressedKey;

            RegisterHotKey(this.Handle, Hotkey_ID, m, k);
            return true;
        }

        private int GetModifierKeysID(Keys keys)
        {
            switch (keys.ToString())
            {
                case "Alt":
                    return 1;
                case "Control":
                    return 2;
                case "Shift":
                    return 4;
                default:
                    return 0;
            }
        }

        private void splitTextButton_Click(object sender, EventArgs e)
        {
            if (text == null)
            {
                MessageBox.Show("Сначала необходимо выбрать текстовый фаил!");
                OpenTextFile();
            }
            for (int i = 0; i < text.Count; i++)
            {
                if (text[i].Length > 80)
                {
                    int lastSpaceIndex = text[i].LastIndexOf(" ");
                    if (lastSpaceIndex > 80)
                    {
                        for (int j = 80; j >= 0; j--)
                        {
                            if (text[i][j] == ' ')
                            {
                                lastSpaceIndex = j;
                                break;
                            }
                        }

                    }
                    string splitted = text[i].Substring(lastSpaceIndex + 1);
                    text[i] = text[i].Substring(0, lastSpaceIndex);
                    text.Insert(i + 1, splitted);
                }
            }
            SetBinderFieldText(text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                File.WriteAllLines(saveFileDialog1.FileName, text);
        }
        private async Task AntiAFK()
        {
            int lastMinutesUpdated = -1;
            while (true)
            {
                if (checkBox1.Checked && (DateTime.Now.Minute - lastMinutesUpdated > (new Random().NextDouble() + 0.1) * 11 || lastMinutesUpdated == -1))
                {
                    await Task.Delay(3000);
                    if (!FindAndSelectGTAWindow()) return;

                    MakeRealisticPress(VirtualKeyCode.ESCAPE);

                    await Task.Delay(700);

                    MakeRealisticPress(VirtualKeyCode.LSHIFT);

                    await Task.Delay(957);

                    MakeRealisticPress(VirtualKeyCode.VK_D, 1000);

                    await Task.Delay(3000);

                    MakeRealisticPress(VirtualKeyCode.ESCAPE);

                    lastMinutesUpdated = DateTime.Now.Minute;

                }
            }
        }
    }
}
