﻿namespace SimpleBinderByFoxPro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.binderPreviousButtonField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textFileName = new System.Windows.Forms.Label();
            this.binderTextField = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.splitTextButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.binderNextButtonField = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.logLabel = new System.Windows.Forms.Label();
            this.enterCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Биндер на одну кнопку";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(847, 487);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(64, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "AntiAFK";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(18, 436);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Выбрать фаил";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // binderPreviousButtonField
            // 
            this.binderPreviousButtonField.Location = new System.Drawing.Point(94, 45);
            this.binderPreviousButtonField.Name = "binderPreviousButtonField";
            this.binderPreviousButtonField.ReadOnly = true;
            this.binderPreviousButtonField.Size = new System.Drawing.Size(83, 20);
            this.binderPreviousButtonField.TabIndex = 3;
            this.binderPreviousButtonField.Text = "Нажми кнопку";
            this.binderPreviousButtonField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.binderButtonPreviousField_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Пред. строка";
            // 
            // textFileName
            // 
            this.textFileName.AutoSize = true;
            this.textFileName.Location = new System.Drawing.Point(15, 487);
            this.textFileName.Name = "textFileName";
            this.textFileName.Size = new System.Drawing.Size(39, 13);
            this.textFileName.TabIndex = 5;
            this.textFileName.Text = "Фаил:";
            // 
            // binderTextField
            // 
            this.binderTextField.HideSelection = false;
            this.binderTextField.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.binderTextField.Location = new System.Drawing.Point(18, 117);
            this.binderTextField.Multiline = true;
            this.binderTextField.Name = "binderTextField";
            this.binderTextField.ReadOnly = true;
            this.binderTextField.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.binderTextField.Size = new System.Drawing.Size(515, 313);
            this.binderTextField.TabIndex = 6;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Text files|*.txt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 436);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Максимальная длина строки - 80 символов на SRP.";
            // 
            // splitTextButton
            // 
            this.splitTextButton.Location = new System.Drawing.Point(401, 452);
            this.splitTextButton.Name = "splitTextButton";
            this.splitTextButton.Size = new System.Drawing.Size(132, 23);
            this.splitTextButton.TabIndex = 8;
            this.splitTextButton.Text = "Разделить текст по 80";
            this.splitTextButton.UseVisualStyleBackColor = true;
            this.splitTextButton.Click += new System.EventHandler(this.splitTextButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "След. строка";
            // 
            // binderNextButtonField
            // 
            this.binderNextButtonField.Location = new System.Drawing.Point(94, 83);
            this.binderNextButtonField.Name = "binderNextButtonField";
            this.binderNextButtonField.ReadOnly = true;
            this.binderNextButtonField.Size = new System.Drawing.Size(83, 20);
            this.binderNextButtonField.TabIndex = 10;
            this.binderNextButtonField.Text = "Нажми кнопку";
            this.binderNextButtonField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.binderNextButtonField_KeyDown);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(401, 487);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Сохранить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "txt";
            this.saveFileDialog1.Filter = "Text files|*.txt";
            // 
            // logLabel
            // 
            this.logLabel.AutoSize = true;
            this.logLabel.Location = new System.Drawing.Point(12, 553);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(28, 13);
            this.logLabel.TabIndex = 12;
            this.logLabel.Text = "Log:";
            // 
            // enterCheckBox
            // 
            this.enterCheckBox.AutoSize = true;
            this.enterCheckBox.Location = new System.Drawing.Point(183, 63);
            this.enterCheckBox.Name = "enterCheckBox";
            this.enterCheckBox.Size = new System.Drawing.Size(51, 17);
            this.enterCheckBox.TabIndex = 13;
            this.enterCheckBox.Text = "Enter";
            this.enterCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 575);
            this.Controls.Add(this.enterCheckBox);
            this.Controls.Add(this.logLabel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.binderNextButtonField);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.splitTextButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.binderTextField);
            this.Controls.Add(this.textFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.binderPreviousButtonField);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox binderPreviousButtonField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label textFileName;
        private System.Windows.Forms.TextBox binderTextField;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button splitTextButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox binderNextButtonField;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label logLabel;
        private System.Windows.Forms.CheckBox enterCheckBox;
    }
}

