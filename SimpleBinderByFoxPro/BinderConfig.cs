﻿using Newtonsoft.Json;
using System.IO;
namespace SimpleBinderByFoxPro
{
    class BinderConfig
    {
        public int m1, k1, m2, k2;

        public BinderConfig(int m1, int k1, int m2, int k2)
        {
            this.m1 = m1;
            this.k1 = k1;
            this.m2 = m2;
            this.k2 = k2;
        }
    }
    class BinderConfigHandler
    {
        public static BinderConfig GetConfig()
        {
            BinderConfig bc;
            try
            {
                bc = JsonConvert.DeserializeObject<BinderConfig>(File.ReadAllText("./config.json"));
            }
            catch
            {
                return null;
            }
            return bc;
        }
        public static void SaveConfig(BinderConfig bc)
        {
            File.WriteAllText("./config.json", JsonConvert.SerializeObject(bc));
        }
    }
}
